import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import numeral from 'numeral';
import selectExpensesTotal from '../selectors/expenses-total';
import { getHiddenExpenses, getVisibleExpenses } from '../selectors/expenses';

export const ExpensesSummary = ({
    visibleExpenseCount,
    expensesTotal,
    hiddenExpenseCount,
}) => {
    const expenseWordVisible =
        visibleExpenseCount === 1 ? 'expense' : 'expenses';
    const expenseWordHidden = hiddenExpenseCount === 1 ? 'expense' : 'expenses';
    const total = numeral(expensesTotal / 100).format('$0,0.00');
    return (
        <div className="page-header">
            <div className="content-container">
                <h1 className="page-header__title">
                    Viewing <span>{visibleExpenseCount}</span>{' '}
                    {expenseWordVisible} totalling <span>{total}</span>
                </h1>
                <h1 className="page-header__subtitle">
                    {' '}
                    *Not showing <span>{hiddenExpenseCount}</span>{' '}
                    {expenseWordHidden} because of filters
                </h1>
                <div className="page-header__actions">
                    <Link className="button" to="/create">
                        Add Expense
                    </Link>
                </div>
            </div>
        </div>
    );
};

const mapStateToProps = state => {
    const visibleExpenses = getVisibleExpenses(state.expenses, state.filters);
    const hiddenExpenses = getHiddenExpenses(state.expenses, state.filters);
    return {
        hiddenExpenseCount: hiddenExpenses.length,
        visibleExpenseCount: visibleExpenses.length,
        expensesTotal: selectExpensesTotal(visibleExpenses),
    };
};

export default connect(mapStateToProps)(ExpensesSummary);
